package model;

public class Armor extends Item {

	protected double defenseValue;

	/**
	 * Armor constructor
	 * @param name name of the armor
	 * @param defenseValue value of the armor
	 * @param posX position in X axis of the armor
	 * @param posY position in Y axis of the armor
	 * @param price the price of the item
	 */
	public Armor(String name, double defenseValue, int posX, int posY, int price) {
		super(name, posX, posY, price);
		this.defenseValue = defenseValue;
	}

	/**
	 * Armor default constructor
	 * @param name name of the armor
	 * @param defenseValue value of the armor
	 */
	public Armor(String name, double defenseValue, int price) {
		super(name, price);
		this.defenseValue = defenseValue;
	}

	/**
	 * Get defenseValue
	 * @return the value of the defense of the armor
	 */
	public double getDefenseValue() {
		return this.defenseValue;
	}

	/**
	 * Set defenseValue
	 * @param defenseValue the value of the armor
	 */
	public void setDefenseValue(int defenseValue) {
		this.defenseValue = defenseValue;
	}

}