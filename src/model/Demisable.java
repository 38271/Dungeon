package model;

public interface Demisable {

	/**
	 * 
	 * @param observer
	 */
	void addObserver(DemisableObserver observer);

	void notifyDemisableObserver();

}