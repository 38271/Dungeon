package model;

public abstract class WizardWeapon extends Weapon {

	/**
	 * WizardWeapon constructor
	 * @param name the name of the WizardWeapon
	 * @param damage damage of the WizardWeapon
	 * @param distance distance the WizardWeapon can reach	
	 * @param posX position in X axis of the WizardWeapon
	 * @param posY position in Y axis of the WizardWeapon
	 * @param price the price of the item
	 */
	public WizardWeapon(String name, double damage, int distance, int posX, int posY, int price) {
		super(name, damage, distance, posX, posY, price);
	}
	
	/**
	 * WizardWeapon constructor in inventory
	 * @param name the name of the WizardWeapon
	 * @param damage damage of the WizardWeapon
	 * @param distance distance the WizardWeapon can reach	
	 * @param price the price of the item
	 */
	public WizardWeapon(String name, double damage, int distance, int price) {
		super(name, damage, distance, price);
	}

}