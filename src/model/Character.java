package model;

import model.Direction;
public abstract class Character extends GameObject implements Demisable {

	protected double life;
	protected Direction direction;

	/**
	 * Character constructor
	 * @param life value of character's life
	 * @param posX the player's character's in the X axis
	 * @param posY the player's character's in the Y axis 
	 * @param direction the direction the character is facing
	 */
	public Character(double life, int posX, int posY, Direction direction) {
		super(posX, posY);
		this.direction = direction;
		this.life = life;
		this.posX = posX;
		this.posY = posY;

	}

	/**
	 * Get life
	 * @return return the value of character's life
	 */
	public double getLife() {
		return this.life;
	}

	/**
	 * Set life
	 * @param life change the value of character's life
	 */
	public void setLife(double life) {
		this.life = life;
	}

	/**
	 * Manage charcater's movement
	 * @param posX the position x of the movement
	 * @param posY the position y of the movement
	 */
	public void move(int posX, int posY) {

	}

	/**
	 * Attack the enemy in front of the character
	 * @param hit the amount of damage dealt
	 */
	public abstract void attack(double hit);

	/**
	 * 
	 * @param observer
	 */
	public void addObserver(DemisableObserver observer) {
		// TODO - implement Character.addObserver
		throw new UnsupportedOperationException();
	}

	public void notifyDemisableObserver() {
		// TODO - implement Character.notifyDemisableObserver
		throw new UnsupportedOperationException();
	}

	/**
	 * Get direction
	 * @return return the direction the character is facing
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * Set direction
	 * @param direction the direction the player is facing
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}