package model;

public class Sword extends WarriorWeapon {

	/**
	 * Sword constructor
	 * @param name the name of the sword
	 * @param damage damage of the sword	
	 * @param distance distance the sword can reach
	 * @param posX position in X axis of the sword
	 * @param posY position in Y axis of the sword
	 * @param price the price of the item
	 */
	public Sword(String name, double damage, int distance, int posX, int posY, int price) {
		super(name, damage, distance, posX, posY, price);
	}

	/**
	 * Sword constructor (first Warrior's weapon)
	 * @param name name of the sword
	 * @param damage damage of the sword
	 * @param distance distance the sword can reach
	 * @param price the price of the item
	 */
	public Sword(String name, double damage, int distance, int price) {
		super(name, damage, distance, price);
	}

}