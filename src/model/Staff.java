package model;

public class Staff extends WizardWeapon {

	/**
	 * Staff constructor
	 * @param name the name of the staff
	 * @param damage damage of the staff	
	 * @param distance distance the staff can reach
	 * @param posX position in X axis of the staff
	 * @param posY position in Y axis of the staff
	 * @param price the price of the item
	 */
	public Staff(String name, double damage, int distance, int posX, int posY, int price) {
		super(name, damage, distance, posX, posY, price);
	}

	/**
	 * Staff constructor (first Wizard's weapon)
	 * @param name name of the staff
	 * @param damage damage of the staff
	 * @param distance distance the staff can reach
	 * @param price the price of the item
	 */
	public Staff(String name, double damage, int distance, int price) {
		super(name, damage, distance, price);
	}

}