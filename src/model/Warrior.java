package model;

public class Warrior extends Player {
	
	private boolean isActive;

	/**
	 * Warrior constructor
	 * @param posX the warrior's position in the X axis
	 * @param posY the warrior's position in the Y axis 
	 * @param direction the direction the warrior is facing
	 */
	public Warrior(int posX, int posY, Direction direction) {
		super(posX, posY, direction);
		Weapon weapon = new Sword("Noob's sword", 15, 1, 5);
		Armor breastplate = new Breastplate("Adventurer's armor", 5, 5);
		this.equipment = new Equipment(weapon, breastplate);
	}
	
	
	/**
	 * Activate the warrior's skill
	 */
	public void activateSkill() {
		if(!isActive){
			activate();
		}
	}

	
	/**
	 * Activate the skill for 5sec
	 */
	private void activate(){
		Thread thread1 = new Thread(new Runnable() {			
			public void run() {
//				System.out.println("--> Lancement du skill pendant 5 sec");
//				System.out.println("d�fense passe de " + defense +" � "+defense*2);
				isActive = true;
				equipment.setDefenseMultiplicator(1.5);
				int i = 0;
				while(i < 5){
					try {
						Thread.sleep(1000);				     
					} catch (InterruptedException e) {
						e.printStackTrace();
					}					
					i++;
//					System.out.println(i);
				}
//				System.out.println("d�fense passe de " + equipment.getDefense() +" � "+defense/2);
				equipment.setDefenseMultiplicator(1);
				waitingTime();
			}
		});
		thread1.start();
	}

	
	/**
	 * Don't allow to activate the skill again within 15 sec
	 */
	private void waitingTime(){
		Thread thread2 = new Thread(new Runnable() {			
			public void run() {
				System.out.println("--> d�but du cooldown pendant 15 sec");
				int i = 0;
				while(i < 15){
					try {
						Thread.sleep(1000);				     
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
					System.out.println(i  +" --> ne peut pas utiliser la comp�tence");
				}

				isActive = false;
				System.out.println("--> peut utiliser le skill s'il appuie sur C");
			}
		});
		thread2.start();
	}

}