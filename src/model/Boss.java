package model;
public class Boss extends Bot {

	private boolean alreadyActivated;
	private final double INIT_LIFE;

	/**
	 * Boss constructor
	 * @param life value of boss's life
	 * @param defense value of boss's defense
	 * @param strength value of boss's strength
	 * @param posX the boss's position in the X axis
	 * @param posY the boss's position in the Y axis 
	 * @param direction the direction the boss is facing
	 */
	public Boss(double life, double defense, double strength, int posX, int posY, Direction direction) {
		super(life, defense, strength, posX, posY, direction);
		INIT_LIFE = life;
	}

	/**
	 * activate the boss's skill
	 */
	public void activateSkill() {
		if(!alreadyActivated){
			System.out.println("d�part "+life);
			alreadyActivated = true;
			life += (INIT_LIFE - life)/1.5 ;
			System.out.println("final "+life);
		}
	}

	@Override
	public void attack(double hit) {
		life -= hit * (1 - defense/200);
	}

	@Override
	public String toString() {
		return "Boss [life = "+ life +"]";
	}

}