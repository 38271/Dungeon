package model;

public class Breastplate extends Armor {
	
	public Breastplate(String name, int defenseValue, int posX, int posY, int price){
		super(name, defenseValue, posX, posY, price);
	}
	
	public Breastplate(String name, int defenseValue, int price){
		super(name, defenseValue, price);
	}

}

