package model;
public interface DemisableObserver {

	/**
	 * 
	 * @param demisable
	 */
	void demise(Demisable demisable);

}