package model;
public interface ItemObserver {

	void update();

}