package model;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

	private int size;
	private List<Item> items;

	/**
	 * Inventory constructor
	 * @param size the size of the inventory
	 */
	public Inventory() {
		this.size = 20;
		this.items = new ArrayList<Item>(size);
	}

	/**
	 * check if the inventory is full or not
	 * @return return true if it is full
	 */
	public boolean isFull() {
		return items.size() >= size;
	}

	/**
	 * Add an item to items
	 * @param item item to add to items
	 */
	public boolean addItem(Item item){
		if(isFull()) {
			return false;
		}		
		return items.add(item);
	}

	/**
	 * Remove an item from items
	 * @param item item to remove from items
	 */
	public void removeItem(Item item){
		items.remove(item);
	}
	
	/**
	 * Get the i Item in the inventory
	 * @param i the index of the list
	 * @return the item
	 */
	public Item getItem(int i){
		return items.get(i);
	}
	
	/**
	 * Get size of inventory
	 * @return return the size of inventory
	 */
	public int getSize() {
		return this.size;
	}

	/**
	 * Set size
	 * @param size size of the inventory
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Get items
	 * @return 
	 */
	public List<Item> getItems() {
		return this.items;
	}

	/**
	 * Set items
	 * @param items
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}
}