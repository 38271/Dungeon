package model;

import java.util.ArrayList;
import java.util.List;

public abstract class Item extends GameObject {

	protected String name;
	protected int price;
	protected static final int INVENTORY = -100;
	private List<ItemObserver> observers;
	
	/**
	 * Item constructor
	 * @param name name of the item
	 * @param posX position in X axis of the item
	 * @param posY position in X axis of the item
	 * @param price the price of the item
	 */
	public Item(String name, int posX, int posY, int price) {
		super(posX, posY);
		this.name = name;
		this.price = price;
	}
	
	/**
	 * Item default constructor
	 * @param name name of the item
	 */
	public Item(String name, int price){
		super();
		this.name = name;
		this.price = price;
	}

	/**
	 * Add the observer
	 * @param observer
	 */
	public void addObserver(ItemObserver observer) {
		if(observers == null){
			observers = new ArrayList<>();
		}
		observers.add(observer);
	}

	public void notifyObserver() {
		if(observers == null)
			return;
		for(ItemObserver obs : observers){
			obs.update();
		}
	}

	/**
	 * Drop the item at (posX,posY)
	 * @param posX position in X axis of item
	 * @param posY position in Y axis of item
	 */
	public void drop(int posX, int posY) {
		this.posX = posX;
		this.posY = posY;
		notifyObserver();
	}

	/**
	 * Collect the item
	 * Set the posX = -100 posY = -100
	 * @param collectable
	 */
	public void collect() {
		posX = -100;
		posY = -100;
		notifyObserver();
	}
	
	public void sell(){
		notifyObserver();
	}
	
	/**
	 * Return true if item is an obstacle
	 */
	public boolean isObstacle() {
		return this.obstacle;
	}

	/**
	 * Get name
	 * @return return the name of the item
	 */
	public final String getName() {
		return this.name;
	}

	/**
	 * Set name
	 * @param name name of the item
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	
	
}