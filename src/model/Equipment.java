package model;

public class Equipment {

	private Weapon weapon;
	private Armor breastplate;
	private Armor necklace;
	private Armor ring;
	private Armor hat;
	private Armor gauntlets;
	private Armor boots;
	private double defenseMultiplicator;
	private double strengthMultiplicator;



	public Equipment(Weapon weapon, Armor breastplate, Armor necklace, Armor ring, Armor hat, Armor gauntlets, Armor boots) {
		this.weapon = weapon;
		this.breastplate = breastplate;
		this.necklace = necklace;
		this.ring = ring;
		this.hat = hat;
		this.gauntlets = gauntlets;
		this.boots = boots;
		this.strengthMultiplicator = 1;
		this.defenseMultiplicator = 1;
	}

	public Equipment(Weapon weapon, Armor breastplate){
		this(weapon, breastplate, null, null, null, null, null);
		
	}

	public double getDefense(){
		double totalDef = 0;
		if(breastplate != null)
			totalDef += breastplate.getDefenseValue();
		if(necklace != null)
			totalDef += necklace.getDefenseValue();
		if(ring != null)
			totalDef += ring.getDefenseValue();
		if(hat != null)
			totalDef += hat.getDefenseValue();
		if(gauntlets != null)
			totalDef += gauntlets.getDefenseValue();
		if(boots != null)
			totalDef += boots.getDefenseValue();

		return totalDef/2 * defenseMultiplicator;
	}
	
	public double getStrength(){
		return weapon.getDamage() * strengthMultiplicator;
	}

	/**
	 * @return the defenseMultiplicator
	 */
	public double getDefenseMultiplicator() {
		return defenseMultiplicator;
	}

	/**
	 * @param defenseMultiplicator the defenseMultiplicator to set
	 */
	public void setDefenseMultiplicator(double defenseMultiplicator) {
		this.defenseMultiplicator = defenseMultiplicator;
	}

	/**
	 * @return the strengthMultiplicator
	 */
	public double getStrengthMultiplicator() {
		return strengthMultiplicator;
	}

	/**
	 * @param d the strengthMultiplicator to set
	 */
	public void setStrengthMultiplicator(double d) {
		this.strengthMultiplicator = d;
	}

	public Weapon getWeapon() {
		return weapon;
	}


	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	/**
	 * @return the breastplate
	 */
	public Armor getBreastplate() {
		return breastplate;
	}

	/**
	 * @param breastplate the breastplate to set
	 */
	public void setBreastplate(Armor breastplate) {
		this.breastplate = breastplate;
	}

	/**
	 * @return the necklace
	 */
	public Armor getNecklace() {
		return necklace;
	}

	/**
	 * @param necklace the necklace to set
	 */
	public void setNecklace(Armor necklace) {
		this.necklace = necklace;
	}

	/**
	 * @return the ring
	 */
	public Armor getRing() {
		return ring;
	}

	/**
	 * @param ring the ring to set
	 */
	public void setRing(Armor ring) {
		this.ring = ring;
	}

	/**
	 * @return the hat
	 */
	public Armor getHat() {
		return hat;
	}

	/**
	 * @param hat the hat to set
	 */
	public void setHat(Armor hat) {
		this.hat = hat;
	}

	/**
	 * @return the gauntlets
	 */
	public Armor getGauntlets() {
		return gauntlets;
	}

	/**
	 * @param gauntlets the gauntlets to set
	 */
	public void setGauntlets(Armor gauntlets) {
		this.gauntlets = gauntlets;
	}

	/**
	 * @return the boots
	 */
	public Armor getBoots() {
		return boots;
	}

	/**
	 * @param boots the boots to set
	 */
	public void setBoots(Armor boots) {
		this.boots = boots;
	}


}