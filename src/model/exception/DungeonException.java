package model.exception;

public class DungeonException extends Exception {

	public DungeonException(String errorMsg){
		super(errorMsg);
	}
}
