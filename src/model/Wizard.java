package model;

public class Wizard extends Player {

	private boolean isActive;

	/**
	 * Wizard constructor
	 * @param posX the wizard's position in the X axis
	 * @param posY the wizard's position in the Y axis 
	 * @param direction the direction the wizard is facing
	 */
	public Wizard(int posX, int posY, Direction direction) {
		super(posX, posY, direction);
		Weapon weapon = new Staff("Staff1", 15, 1, 5);
		Armor breastplate = new Breastplate("Adventurer's cap", 5, 5);
		this.equipment = new Equipment(weapon, breastplate);
	}

	
	/**
	 * Activate the wizard's skill
	 */
	public void activateSkill() {
		if(!isActive){
			activate();
		}
	}

	
	/**
	 * Activate the skill for 5sec
	 */
	private void activate(){
		Thread thread1 = new Thread(new Runnable() {			
			public void run() {
				isActive = true;
				equipment.setStrengthMultiplicator(1.5);
				int i = 0;
				while(i < 5){
					try {
						Thread.sleep(1000);				     
					} catch (InterruptedException e) {
						e.printStackTrace();
					}					
					i++;
				}
				equipment.setStrengthMultiplicator(1);

				waitingTime();
			}
		});
		thread1.start();
	}
	
	
	/**
	 * Don't allow to activate the skill again within 15 sec
	 */
	private void waitingTime(){
		Thread thread2 = new Thread(new Runnable() {			
			public void run() {
				int i = 0;
				while(i < 15){
					try {
						Thread.sleep(1000);				     
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
				}
				isActive = false;
			}
		});
		thread2.start();
	}
}