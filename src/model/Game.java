package model;

import java.util.List;

import view.Window;

public class Game implements ItemObserver, DemisableObserver {

	private Player player;
	private List<GameObject> gameObjects;//list of all game object in the map (Player, Items, Bots, Door)
	private Window window;
	private Door door;
	
	/**
	 * 
	 * @param window
	 */
	public Game(Window window) {
		this.window = window; 
		init();
	}

	/**
	 * Initialization of the game objects in the Game
	 */
	public void init(){//TODO on hardcode tous les objet et inchallah ibrahim va trouver la methode de generation automatique de la map
		Item potion = new Potion("Invincible", 15, 10, 10, 5);//truc qui d�marre
		gameObjects.add(potion);
	}
	
	/**
	 * 
	 * @param demisable
	 */
	public void demise(Demisable demisable) {
		if (demisable instanceof Player){
			//TODO quitter la partie et sauvegarder les coins pour pouvoir laisser ibrahim developper cette partie
		}else if (demisable instanceof Bot){
			//TODO verifier si il nyen a plus pour ouvrir la porte en plus de les supprimer de la map
				gameObjects.remove(demisable);
				for(GameObject object : gameObjects){
					if(object instanceof Bot){
						return;
					}
				}
				door.open();
		}
		//refresh la fenetre
	}

	/**
	 * 
	 * @param posX
	 * @param posY
	 */
	public void movePlayer(int posX, int posY) {
		// TODO - implement Game.movePlayer
		throw new UnsupportedOperationException();
	}

	@Override
	public void update() {
		// impl�mente la m�thode de ItemObserver et qui va redessiner la map
		
	}
	
	public Player getPlayer() {
		return this.player;
	}

	/**
	 * 
	 * @param player
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	public List<GameObject> getGameObjects() {
		return this.gameObjects;
	}

	/**
	 * 
	 * @param gameObjects
	 */
	public void setGameObjects(List<GameObject> gameObjects) {
		this.gameObjects = gameObjects;
	}

	public void use() {
		Item selectedItem = player.getInventory().getItem(0);//on a pas implementer la selection donc toujour le 1er
		
		if(selectedItem instanceof Potion){
			player.heal(((Potion)selectedItem).getHeal());
			player.getInventory().removeItem(selectedItem);//potion supprim�e de l'inventaire
		}
		else if(selectedItem instanceof Weapon)
			player.equip(selectedItem);
		
		else if(selectedItem instanceof Armor)
			player.equip(selectedItem);
		
	}
	
	public void sell(){
		
	}
	
	public void collect(){
		
	}
	
	public void drop(){
		
	}

	public void attack(){
		
	}

}