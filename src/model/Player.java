package model;

import model.Inventory;

public abstract class Player extends Character {

	protected Inventory inventory;
	protected Equipment equipment;
	protected int coins;
	private static final double MAX_LIFE = 300;

	/**
	 * Player constructor
	 * @param posX the player's position in the X axis
	 * @param posY the player's position in the Y axis 
	 * @param direction the direction the player is facing
	 */
	public Player(int posX, int posY, Direction direction) {
		super(MAX_LIFE, posX, posY, direction);
		this.inventory = new Inventory();
		this.coins = 0;
	}

	/**
	 * heal the player
	 * @param heal
	 */
	public void heal(int heal){
		this.life += heal;
	}

	/**
	 * equip the player with the item
	 * @param item item to be equipped
	 */
	public void equip(Item item){//TODO verifier si je suis equiper avent de switcher !!!
		if(item instanceof Weapon){
			if(equipment.getWeapon() != null){
				Weapon weapon = equipment.getWeapon();//save worn weapon (weapon)
				equipment.setWeapon((Weapon)item);//equip the weapon (item)
				switchEquipment(item, weapon);
			}else{
				equipment.setWeapon((Weapon)item);
				inventory.removeItem(item);
			}
		}
		if(item instanceof Breastplate){
			Armor breastplate = equipment.getBreastplate();
			equipment.setBreastplate((Breastplate)item);
			switchEquipment(item, breastplate);
		}
		if(item instanceof Boots){
			Armor boots = equipment.getBoots();
			equipment.setBoots((Boots)item);
			switchEquipment(item, boots);
		}
		if(item instanceof Necklace){
			Armor necklace = equipment.getNecklace();
			equipment.setNecklace((Necklace)item);
			switchEquipment(item, necklace);
		}
		if(item instanceof Ring){
			Armor ring = equipment.getRing();
			equipment.setRing((Ring)item);
			switchEquipment(item, ring);
		}
		if(item instanceof Hat){
			Armor hat = equipment.getHat();
			equipment.setHat((Hat)item);
			switchEquipment(item, hat);
		}
		if(item instanceof Gauntlets){
			Armor gauntlets = equipment.getGauntlets();
			equipment.setGauntlets((Gauntlets)item);
			switchEquipment(item, gauntlets);
		}
	}

	/**
	 * Switch the worn equipment by the new one from the inventory
	 * @param newItem item in the inventory to be removed
	 * @param oldItem item in the inventory to be add
	 */
	private void switchEquipment(Item newItem, Item oldItem){
		inventory.removeItem(newItem);
		inventory.addItem(oldItem);
	}

	public void sell(Item item, int price){
		inventory.removeItem(item);
		coins += price;
	}

	public void attack(double hit){
		if(hit > this.getDefense())
			this.life -= hit - this.getDefense();
	}

	public double getDefense(){
		return equipment.getDefense();
	}

	public double getStrength(){
		return equipment.getStrength();
	}

	/**
	 * Get inventory
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}

	/**
	 * Set inventory
	 * @param inventory players'inventory
	 */
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	/**
	 * @return the equipment
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * @param equipment the equipment to set
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	/**
	 * Get coinBag
	 * @return the coinBag
	 */
	public int getCoinBag() {
		return coins;
	}

	/**
	 * Set coinBag
	 * @param coinBag the coinBag to set
	 */
	public void setCoinBag(int coinBag) {
		this.coins = coinBag;
	}

	public abstract void activateSkill();


}