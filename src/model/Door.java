package model;
public class Door extends GameObject {

	private boolean open;

	/**
	 * Door constructor
	 * @param posX the position in the X axis of the Door
	 * @param posY the position in the Y axis of the Door
	 */
	public Door(int posX, int posY) {
		super(posX, posY);
	}

	/**
	 * Open the door
	 */
	public void open(){
		setOpen(true);
	}
	
	/**
	 * Check if the door is open or not
	 * @return true if the door is open, false otherwise
	 */
	public boolean isOpen() {
		return this.open;
	}

	/**
	 * Open/close the door and set obstacle to false/true
	 * @param open true to open and false to close
	 */
	public void setOpen(boolean open) {
		this.open = open;
		this.obstacle = !open;
	}

} 