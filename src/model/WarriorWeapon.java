package model;

public abstract class WarriorWeapon extends Weapon {

	/**
	 * WarriorWeapon constructor
	 * @param name the name of the WarriorWeapon
	 * @param damage damage of the WarriorWeapon
	 * @param distance distance the WarriorWeapon can reach	
	 * @param posX position in X axis of the WarriorWeapon
	 * @param posY position in Y axis of the WarriorWeapon
	 * @param price the price of the item
	 */
	public WarriorWeapon(String name, double damage, int distance, int posX, int posY, int price) {
		super(name, damage, distance, posX, posY, price);
	}
	
	/**
	 * WarriorWeapon constructor
	 * @param name the name of the WarriorWeapon
	 * @param damage damage of the WarriorWeapon
	 * @param distance distance the WarriorWeapon can reach	
	 * @param price the price of the item
	 */
	public WarriorWeapon(String name, double damage, int distance, int price) {
		super(name, damage, distance, price);
	}

}