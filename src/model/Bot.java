package model;
public class Bot extends Character {
	double defense;
	double strength;

	/**
	 * Slime constructor
	 * @param life value of slime's life
	 * @param defense value of slime's defense
	 * @param strength value of slime's strength
	 * @param posX the slime's position in the X axis
	 * @param posY the slime's position in the Y axis 
	 * @param direction the direction the slime is facing
	 */
	public Bot(double life, double defense, double strength, int posX, int posY, Direction direction) {
		super(life, posX, posY, direction);
		this.defense = defense;
		this.strength = strength;
	}

	@Override
	public void attack(double hit) {
		life -= hit * (1 - defense/70);
	}
	
	/**
	 * @return the defense
	 */
	public double getDefense() {
		return defense;
	}

	/**
	 * @param defense the defense to set
	 */
	public void setDefense(double defense) {
		this.defense = defense;
	}

	/**
	 * @return the strength
	 */
	public double getStrength() {
		return strength;
	}

	/**
	 * @param strength the strength to set
	 */
	public void setStrength(double strength) {
		this.strength = strength;
	}


}