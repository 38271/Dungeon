package view;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import model.GameObject;

public class Window extends JFrame{
	
	private Map map = new Map();
	

	/**
	 * 
	 * @param height
	 * @param width
	 */
	public Window(int height, int width) {
		this.setResizable(false);
		JFrame window = new JFrame("Game");
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    window.setSize(650, 650);
	    window.setContentPane(new MainMenu(this));
	    window.getContentPane().add(this.map);
	    window.setVisible(true);
	}

	public void update() {
		this.map.redraw();		
	}

	/**
	 * 
	 * @param gameObjects
	 */
	public void setGameObjects(List<GameObject> gameObjects) {
	}

	/**
	 * 
	 * @param keyboard
	 */
	public void setKeyListener(KeyListener keyboard) {
		this.map.addKeyListener(keyboard);
	}

}