package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainMenu extends JPanel {
	
	Integer[] tabSize = {5, 6};
	String[] tabClass = {"Warrior", "Wizard"};
	JComboBox<String> playerClass = new JComboBox<String>(tabClass);
	JLabel labelClass = new JLabel("Player Type");
	JComboBox<Integer> sizeMap = new JComboBox<Integer>(tabSize);
	JLabel labelSize = new JLabel("Map Size");
	Image finalfantasy = new ImageIcon("ff.jpeg").getImage();
	JButton continueButton = new JButton("Continue");
	JButton startButton = new JButton("New Game");
	JButton shop = new JButton("SHOP");
	Font police = new Font("Century", Font.BOLD,20);
	JPanel north = new JPanel();
	JPanel south = new JPanel();
	
	public MainMenu (Window window){
		this.setLayout(new BorderLayout());
		labelClass.setFont(police);
		labelSize.setFont(police);
		labelClass.setForeground(Color.BLUE);
		labelSize.setForeground(Color.BLUE);
		north.setBackground(Color.WHITE);
		north.add(labelSize);
		north.add(sizeMap);
		north.add(labelClass);
		north.add(playerClass);
		this.add(north, BorderLayout.NORTH);
		sizeMap.setPreferredSize(new Dimension(100, 25));
		sizeMap.setForeground(Color.BLUE);
	    playerClass.setPreferredSize(new Dimension(100, 25));
	    playerClass.setForeground(Color.BLUE);
	    south.add(continueButton);
	    south.add(startButton);
	    continueButton.setPreferredSize(new Dimension(100, 25));  
	    //continueButton.addActionListener(new Startgame(comboD,player,wind,false));
	    startButton.setPreferredSize(new Dimension(100, 25));  
	    //startButton.addActionListener(new Startgame(comboD,player,wind,true));
	    shop.setPreferredSize(new Dimension(100, 25));
	    //action
		
	}
	
	public void paintComponent(Graphics g){
		//super.paintComponent(g);
		g.drawImage(finalfantasy, 0, 0, this.getWidth(), this.getHeight(),this);
	}


	

	
	

}
